<?php

namespace Soong\Tests\Loader;

use Soong\Tests\Contracts\KeyMap\KeyMapTestBase;
use Soong\Tests\DBAL\DBALTesting;

/**
 * Tests the \Soong\KeyMap\DBAL class.
 */
class DBALKeyMapTest extends KeyMapTestBase
{
    use DBALTesting;

    /**
     * @inheritdoc
     */
    protected function setUp() : void
    {
        parent::setUp();
        $this->keyMapClass = '\Soong\KeyMap\DBAL';
        $this->dbSetup();
        $this->configuration['table'] = 'test_key_map_table';
    }
}
