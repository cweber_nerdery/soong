<?php

namespace Soong\Tests\Contracts\Extractor;

/**
 * Base class for testing CountableExtractor implementations.
 *
 * Adds count() testing to ExtractorTestBase.
 */
abstract class CountableExtractorTestBase extends ExtractorTestBase
{

    /**
     * Test count().
     *
     * @dataProvider extractAllDataProvider
     *
     * @param array $configuration
     *   Extractor configuration.
     * @param mixed $expected
     *   Expected set of data records returned.
     */
    public function testCount(array $configuration, $expected)
    {
        $extractor = ($this->extractorClass)::create($configuration);
        $this->assertEquals(count($expected), $extractor->count());
    }
}
