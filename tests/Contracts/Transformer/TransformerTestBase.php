<?php

namespace Soong\Tests\Contracts\Transformer;

use PHPUnit\Framework\TestCase;
use Soong\Contracts\Data\DataProperty;
use Soong\Contracts\Transformer\Transformer;

/**
 * Base class for testing Transformer implementations.
 *
 * To test a transformer class, extend this class and implement setUp(),
 * assigning the fully-qualified class name to transformerClass:
 *
 * @code
 *     protected function setUp() : void
 *     {
 *         $this->transformerClass = '\Soong\Transformer\Copy';
 *     }
 * @endcode
 *
 * And implement a data provider, where each row of data provided contains a
 * configuration array for the transformer, a source value to pass in, and the
 * expected result:
 *
 * @code
 *     public function transformerDataProvider()
 *     {
 *         return [
 *             'empty configuration' => [[], 'source_value', 'expected_value'],
 *             'option1 set' => [['option1' => 'setting1', 5, 8],
 *         ];
 *     }
 * @endcode
 */
abstract class TransFormerTestBase extends TestCase
{

    /**
     * Fully-qualified name of a Transformer implementation.
     *
     * @var Transformer $transformerClass
     */
    protected $transformerClass;

    /**
     * Test transform().
     *
     * @dataProvider transformerDataProvider
     *
     * @param array $configuration
     *   Transformer configuration.
     * @param mixed $source
     *   Source value to be transformed.
     * @param mixed $expected
     *   Expected result of the transformation.
     */
    public function testTransformer(array $configuration, $source, $expected)
    {
        $transformer = ($this->transformerClass)::create($configuration);
        $property = $this->createMock(DataProperty::class);
        $property->method('getValue')->willReturn($source);
        /** @var DataProperty $property */
        $result = $transformer->transform($property);
        $this->assertEquals($expected, $result->getValue(), "{$this->transformerClass} transformed");
    }
}
