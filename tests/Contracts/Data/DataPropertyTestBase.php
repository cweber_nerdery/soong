<?php

namespace Soong\Tests\Contracts\Data;

use PHPUnit\Framework\TestCase;
use Soong\Contracts\Data\DataProperty;

/**
 * Base class for testing DataProperty implementations.
 *
 * To test a property class, simply extend this class and implement
 * setUp(), assigning the fully-qualified class name to propertyClass:
 *
 * @code
 *     protected function setUp() : void
 *     {
 *         $this->propertyClass = '\Soong\Data\Property';
 *     }
 * @endcode
 */
abstract class DataPropertyTestBase extends TestCase
{

    /**
     * Fully-qualified name of a DataProperty implementation.
     *
     * @var DataProperty $propertyClass
     */
    protected $propertyClass;

    /**
     * Provides a bunch of data types to test.
     *
     * @return array
     */
    public function propertyDataProvider() : array
    {
        return [
            'positive integer' => [1, false],
            'negative integer' => [-1, false],
            'numeric string' => ['1', false],
            'zero' => [0, false],
            'empty string' => ['', false],
            'non-empty string' => ['a string', false],
            'float' => [1.2345, false],
            'null' => [null, true],
            'empty array' => [[], false],
            'non-empty array' => [[1, 2, 'five'], false],
            'keyed array' => [['a' => 'b', 'c' => 'd'], false],
            'object' => [new \stdClass(), false],
            'true' => [true, false],
            'false' => [false, false],
        ];
    }

    /**
     * Test getProperty() and isEmpty().
     *
     * @dataProvider propertyDataProvider
     *
     * @param $value
     *   The value to be passed to (and retrieved from) the property.
     * @param $isEmpty
     *   The expected return from isEmpty().
     */
    public function testProperty($value, $isEmpty)
    {
        $property = ($this->propertyClass)::create($value);
        $this->assertEquals($value, $property->getValue(), "Property retrieved");
        $this->assertEquals(gettype($value), gettype($property->getValue()), "Property maintains its type");
        $this->assertEquals($isEmpty, $property->isEmpty(), "Property emptiness identified");
    }
}
