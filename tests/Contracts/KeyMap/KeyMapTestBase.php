<?php

namespace Soong\Tests\Contracts\KeyMap;

use PHPUnit\Framework\TestCase;
use Soong\Contracts\KeyMap\KeyMap;
use Soong\KeyMap\DBAL;

/**
 * Base class for testing KeyMap implementations.
 *
 * To test a keymap class, extend this class and implement setUp(), assigning
 * the fully-qualified class name to keyMapClass:
 *
 * @code
 *     protected function setUp() : void
 *     {
 *         $this->keyMapClass = '\Soong\KeyMap\DBAL';
 *         // Populate any other setup needed for the particular implementation.
 *         $this->configuration['table'] = 'test_table';
 *     }
 * @endcode
 */
abstract class KeyMapTestBase extends TestCase
{

    /**
     * Fully-qualified name of a KeyMap implementation.
     *
     * @var KeyMap $keyMapClass
     */
    protected $keyMapClass;

    /**
     * Configuration for the KeyMap object.
     *
     * @var array
     */
    protected $configuration = [];

    /**
     * Test lookupLoadedKey().
     *
     * @dataProvider lookupLoadedKeyDataProvider
     *
     * @param array $keyMetadata
     *   Definitions of the properties for the extracted and loaded keys.
     * @param array $mapData
     *   Mappings of extracted to loaded keys to populate the key map.
     * @param array $extractedKeysToLookup
     *   The extracted keys we want to lookup.
     * @param array $expectedLoadedKeys
     *   The loaded keys we expect to come back.
     */
    public function testLookupLoadedKey(
        array $keyMetadata,
        array $mapData,
        array $extractedKeysToLookup,
        array $expectedLoadedKeys
    ) : void {
    
        // Populate the key map.
        $configuration = array_merge($this->configuration, $keyMetadata);
        $keyMap = ($this->keyMapClass)::create($configuration);
        foreach ($mapData as $keyList) {
            $keyMap->saveKeyMap($keyList['extracted_key'], $keyList['loaded_key']);
        }

        // Check each key pair.
        foreach ($extractedKeysToLookup as $index => $extractedKey) {
            $loadedKey = $keyMap->lookupLoadedKey($extractedKey);
            $this->assertEquals($expectedLoadedKeys[$index], $loadedKey);
        }
    }

    /**
     * Test data for looking up loaded keys from extracted keys.
     *
     * @return array
     *   Each array member is a named data set containing:
     *     key_metadata: Extractor/loader key names and types.
     *     map_data: Array of data to be saved in the keymap, containing an
     *        array representing a unique key of extracted data and an array
     *        representing a unique key of loaded data.
     *     extracted_keys_to_lookup: Array of key arrays to lookup.
     *     expected_loaded_keys: Array representing the expected loaded data key
     *        to be returned for each extracted_keys_to_lookup.
     */
    public function lookupLoadedKeyDataProvider() : array
    {
        return [
            'single-column keys' => [
                'key_metadata' => [
                    DBAL::CONFIGURATION_EXTRACTOR_KEYS => [
                        'sourceid' => ['type' => 'integer'],
                    ],
                    DBAL::CONFIGURATION_LOADER_KEYS => [
                        'destid' => ['type' => 'integer'],
                    ],
                ],
                'map_data' => [
                    ['extracted_key' => [1], 'loaded_key' => [2]],
                    ['extracted_key' => [4], 'loaded_key' => []],
                ],
                'extracted_keys_to_lookup' => [
                    [1],
                    [3],
                    [4],
                ],
                'expected_loaded_keys' => [
                    ['destid' => 2],
                    [],
                    ['destid' => null],
                ],
            ],
            'multi-column source' => [
                'key_metadata' => [
                    DBAL::CONFIGURATION_EXTRACTOR_KEYS => [
                        'sourceid1' => ['type' => 'integer'],
                        'sourceid2' => ['type' => 'integer']
                    ],
                    DBAL::CONFIGURATION_LOADER_KEYS => [
                        'destid' => ['type' => 'integer'],
                    ],
                ],
                'map_data' => [
                    ['extracted_key' => [1, 2], 'loaded_key' => [3]],
                    ['extracted_key' => [4, 5], 'loaded_key' => []],
                ],
                'extracted_keys_to_lookup' => [
                    [1, 2],
                    [1, 5],
                    [4, 5],
                ],
                'expected_loaded_keys' => [
                    ['destid' => 3],
                    [],
                    ['destid' => null],
                ],
            ],
            'multi-column dest' => [
                'key_metadata' => [
                    DBAL::CONFIGURATION_EXTRACTOR_KEYS => [
                        'sourceid' => ['type' => 'integer'],
                    ],
                    DBAL::CONFIGURATION_LOADER_KEYS => [
                        'destid1' => ['type' => 'integer'],
                        'destid2' => ['type' => 'integer'],
                    ],
                ],
                'map_data' => [
                    ['extracted_key' => [1], 'loaded_key' => [2, 3]],
                    ['extracted_key' => [4], 'loaded_key' => []],
                ],
                'extracted_keys_to_lookup' => [
                    [1],
                    [4],
                    [5],
                ],
                'expected_loaded_keys' => [
                    ['destid1' => 2, 'destid2' => 3],
                    ['destid1' => null, 'destid2' => null],
                    [],
                ],
            ],
        ];
    }

    /**
     * Test lookupExtractedKeys().
     *
     * @dataProvider lookupExtractedKeysDataProvider
     *
     * @param array $keyMetadata
     *   Definitions of the properties for the extracted and loaded keys.
     * @param array $mapData
     *   Mappings of extracted to loaded keys to populate the key map.
     * @param array $loadedKeysToLookup
     *   The loaded keys we want to lookup.
     * @param array $expectedExtractedKeys
     *   The extracted keys we expect to come back.
     */
    public function testLookupExtractedKeys(
        array $keyMetadata,
        array $mapData,
        array $loadedKeysToLookup,
        array $expectedExtractedKeys
    ) : void {
    
        // Populate the key map.
        $configuration = array_merge($this->configuration, $keyMetadata);
        $keyMap = ($this->keyMapClass)::create($configuration);
        foreach ($mapData as $keyList) {
            $keyMap->saveKeyMap($keyList['extracted_key'], $keyList['loaded_key']);
        }

        // Check each key pair.
        foreach ($loadedKeysToLookup as $index => $loadedKey) {
            $extractedKey = $keyMap->lookupExtractedKeys($loadedKey);
            $this->assertEquals($expectedExtractedKeys[$index], $extractedKey);
        }
    }

    /**
     * Test data for looking up extracted keys from loaded keys.
     *
     * @return array
     *   Each array member is a named data set containing:
     *     key_metadata: Extractor/loader key names and types.
     *     map_data: Array of data to be saved in the keymap, containing an
     *        array representing a unique key of extracted data and an array
     *        representing a unique key of loaded data.
     *     loaded_keys_to_lookup: Array of key arrays to lookup.
     *     expected_extracted_keys: Array representing the expected extracted
     *        data key to be returned for each loaded_keys_to_lookup.
     */
    public function lookupExtractedKeysDataProvider() : array
    {
        return [
            'single-column keys' => [
                'key_metadata' => [
                    DBAL::CONFIGURATION_EXTRACTOR_KEYS => [
                        'sourceid' => ['type' => 'integer'],
                    ],
                    DBAL::CONFIGURATION_LOADER_KEYS => [
                        'destid' => ['type' => 'integer'],
                    ],
                ],
                'map_data' => [
                    ['extracted_key' => [1], 'loaded_key' => [2]],
                    ['extracted_key' => [3], 'loaded_key' => [2]],
                    ['extracted_key' => [4], 'loaded_key' => [5]],
                ],
                'loaded_keys_to_lookup' => [
                    [5],
                    [6],
                    [2],
                ],
                'expected_extracted_keys' => [
                    [['sourceid' => 4]],
                    [],
                    [['sourceid' => 1], ['sourceid' => 3]],
                ],
            ],
            'multi-column source' => [
                'key_metadata' => [
                    DBAL::CONFIGURATION_EXTRACTOR_KEYS => [
                        'sourceid1' => ['type' => 'integer'],
                        'sourceid2' => ['type' => 'integer']
                    ],
                    DBAL::CONFIGURATION_LOADER_KEYS => [
                        'destid' => ['type' => 'integer'],
                    ],
                ],
                'map_data' => [
                    ['extracted_key' => [1, 2], 'loaded_key' => [3]],
                    ['extracted_key' => [4, 5], 'loaded_key' => [3]],
                    ['extracted_key' => [6, 7], 'loaded_key' => [8]],
                ],
                'loaded_keys_to_lookup' => [
                    [3],
                    [8],
                    [9],
                ],
                'expected_extracted_keys' => [
                    [
                        ['sourceid1' => 1, 'sourceid2' => 2],
                        ['sourceid1' => 4, 'sourceid2' => 5],
                    ],
                    [['sourceid1' => 6, 'sourceid2' => 7]],
                    [],
                ],
            ],
            'multi-column dest' => [
                'key_metadata' => [
                    DBAL::CONFIGURATION_EXTRACTOR_KEYS => [
                        'sourceid' => ['type' => 'integer'],
                    ],
                    DBAL::CONFIGURATION_LOADER_KEYS => [
                        'destid1' => ['type' => 'integer'],
                        'destid2' => ['type' => 'integer'],
                    ],
                ],
                'map_data' => [
                    ['extracted_key' => [1], 'loaded_key' => [2, 3]],
                    ['extracted_key' => [4], 'loaded_key' => [2, 3]],
                    ['extracted_key' => [5], 'loaded_key' => [6, 7]],
                ],
                'loaded_keys_to_lookup' => [
                    [2, 3],
                    [6, 7],
                    [8, 9],
                ],
                'expected_extracted_keys' => [
                    [
                        ['sourceid' => 1],
                        ['sourceid' => 4],
                    ],
                    [['sourceid' => 5]],
                    [],
                ],
            ],
        ];
    }

    /**
     * Test delete().
     *
     * @dataProvider deleteDataProvider
     *
     * @param array $keyMetadata
     *   Definitions of the properties for the extracted and loaded keys.
     * @param array $mapData
     *   Mappings of extracted to loaded keys to populate the key map.
     * @param array $keysToDelete
     *   The extracted keys we want to delete.
     */
    public function testDelete(array $keyMetadata, array $mapData, array $keysToDelete) : void
    {
        // Populate the key map.
        $configuration = array_merge($this->configuration, $keyMetadata);
        $keyMap = ($this->keyMapClass)::create($configuration);
        foreach ($mapData as $keyList) {
            $keyMap->saveKeyMap($keyList['extracted_key'], $keyList['loaded_key']);
        }

        // Check each key pair.
        foreach ($keysToDelete as $extractedKey) {
            $loadedKey = $keyMap->lookupLoadedKey($extractedKey);
            $this->assertNotEmpty($loadedKey);
            $keyMap->delete($extractedKey);
            $loadedKey = $keyMap->lookupLoadedKey($extractedKey);
            $this->assertEmpty($loadedKey);
        }
    }

    /**
     * Test data for deleting map entries.
     *
     * @return array
     *   Each array member is a named data set containing:
     *     key_metadata: Extractor/loader key names and types.
     *     map_data: Array of data to be saved in the keymap, containing an
     *        array representing a unique key of extracted data and an array
     *        representing a unique key of loaded data.
     *     keys_to_delete: Array of key arrays to delete.
     */
    public function deleteDataProvider() : array
    {
        return [
            'single-column keys' => [
                'key_metadata' => [
                    DBAL::CONFIGURATION_EXTRACTOR_KEYS => [
                        'sourceid' => ['type' => 'integer'],
                    ],
                    DBAL::CONFIGURATION_LOADER_KEYS => [
                        'destid' => ['type' => 'integer'],
                    ],
                ],
                'map_data' => [
                    ['extracted_key' => [1], 'loaded_key' => [2]],
                    ['extracted_key' => [4], 'loaded_key' => []],
                ],
                'keys_to_delete' => [
                    [1],
                    [4],
                ],
            ],
            'multi-column source' => [
                'key_metadata' => [
                    DBAL::CONFIGURATION_EXTRACTOR_KEYS => [
                        'sourceid1' => ['type' => 'integer'],
                        'sourceid2' => ['type' => 'integer']
                    ],
                    DBAL::CONFIGURATION_LOADER_KEYS => [
                        'destid' => ['type' => 'integer'],
                    ],
                ],
                'map_data' => [
                    ['extracted_key' => [1, 2], 'loaded_key' => [3]],
                    ['extracted_key' => [4, 5], 'loaded_key' => []],
                ],
                'keys_to_delete' => [
                    [1, 2],
                    [4, 5],
                ],
            ],
            'multi-column dest' => [
                'key_metadata' => [
                    DBAL::CONFIGURATION_EXTRACTOR_KEYS => [
                        'sourceid' => ['type' => 'integer'],
                    ],
                    DBAL::CONFIGURATION_LOADER_KEYS => [
                        'destid1' => ['type' => 'integer'],
                        'destid2' => ['type' => 'integer'],
                    ],
                ],
                'map_data' => [
                    ['extracted_key' => [1], 'loaded_key' => [2, 3]],
                    ['extracted_key' => [4], 'loaded_key' => []],
                ],
                'keys_to_delete' => [
                    [1],
                    [4],
                ],
            ],
        ];
    }

    /*
      The KeyMap should itself be an iterator, test when that is implemented.
    public function testIterate()
    {

    }*/
}
