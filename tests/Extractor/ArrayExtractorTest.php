<?php

namespace Soong\Tests\Extractor;

use Soong\Tests\Contracts\Extractor\CountableExtractorTestBase;

/**
 * Tests the \Soong\Extractor\ArrayExtractor class.
 */
class ArrayExtractorTest extends CountableExtractorTestBase
{

    /**
     * Specify the class we're testing.
     */
    protected function setUp() : void
    {
        $this->extractorClass = '\Soong\Extractor\ArrayExtractor';
    }

    /**
     * Basic test data.
     *
     * @return array
     */
    private function data() : array
    {
        return [
            [
                'positive integer' => 11,
                'negative integer' => -23,
                'numeric string' => '563',
                'zero' => 0,
                'empty string' => '',
                'non-empty string' => 'a string',
                'float' => 1.2345,
                'null' => null,
                'empty array' => [],
                'non-empty array' => [1, 2, 'five'],
                'keyed array' => ['a' => 'b', 'c' => 'd'],
                'object' => new \stdClass(),
                'true' => true,
                'false' => false,
            ],
        ];
    }

    /**
     * Key property configuration.
     *
     * @return array
     */
    private function keyProperties() : array
    {
        return [
            'positive integer' => [
                'type' => 'integer',
            ]
        ];
    }

    /**
     * Basic extractor configuration for testing.
     *
     * @return array
     */
    private function configuration() : array
    {
        return [
            // @todo replace with mock
            'data_record_class' => 'Soong\Data\Record',
            'key_properties' => $this->keyProperties(),
            'data' => $this->data(),
        ];
    }

    /**
     * Test extraction of various types of values
     *
     * @return array
     */
    public function extractAllDataProvider() : array
    {
        return [
            'set one' => [$this->configuration(), $this->data()],
        ];
    }

    /**
     * Test retrieval of property metadata.
     *
     * @return array
     */
    public function propertyDataProvider() : array
    {
        $properties = array_keys($this->data()[0]);
        return [
            'set one' => [$this->configuration(), $properties, $this->keyProperties()],
        ];
    }
}
