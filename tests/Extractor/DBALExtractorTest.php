<?php

namespace Soong\Tests\Extractor;

use Soong\Tests\Contracts\Extractor\CountableExtractorTestBase;
use Soong\Tests\DBAL\DBALTesting;

/**
 * Tests the \Soong\Extractor\DBAL class.
 */
class DBALExtractorTest extends CountableExtractorTestBase
{
    use DBALTesting;

    /**
     * @inheritdoc
     */
    protected function setUp() : void
    {
        parent::setUp();
        $this->extractorClass = '\Soong\Extractor\DBAL';
        $this->dbSetup();
    }

    /**
     * Test data with db setup.
     *
     * @return array
     *   Each value is an array representing one data set:
     *     table_name: Name of the table to use for the test data.
     *     sql: Array of SQL statements to create and populate test tables.
     *     key_properties: Schema of table key properties as passed in
     *       extractor configuration.
     *     data: Array of data rows to store in the table. Values in each row
     *       are keyed by column name.
     */
    protected function data() : array
    {
        $data['various column types'] = [
            'table_name' => 'soong_dbal_extractor_test_various_types',
            'sql' => [
                'CREATE TABLE soong_dbal_extractor_test_various_types (positive INTEGER NOT NULL, negative INTEGER NOT NULL, numeric VARCHAR(255) NOT NULL, zero INTEGER NOT NULL, emptystring VARCHAR(255) NOT NULL, float DOUBLE PRECISION NOT NULL, string VARCHAR(255) NOT NULL, nullfield VARCHAR(255) DEFAULT NULL, PRIMARY KEY(positive))',
                "INSERT INTO soong_dbal_extractor_test_various_types VALUES(11,-23,'563',0,'',1.2344999999999999307,'a string',NULL)",
            ],
            'key_properties' => [
                'positive' => [
                    'type' => 'integer',
                ]
            ],
            'data' => [
                [
                    'positive' => 11,
                    'negative' => -23,
                    'numeric' => '563',
                    'zero' => 0,
                    'emptystring' => '',
                    'string' => 'a string',
                    'float' => 1.2345,
                    'nullfield' => null,
                ],
            ],
        ];
        $data['empty table'] = [
            'table_name' => 'soong_dbal_extractor_test_empty_table',
            'sql' => [
                'CREATE TABLE soong_dbal_extractor_test_empty_table (id INTEGER NOT NULL, PRIMARY KEY(id))',
            ],
            'key_properties' => [
                'id' => [
                    'type' => 'integer',
                ]
            ],
            'data' => [],
        ];
        $data['multiple rows'] = [
            'table_name' => 'soong_dbal_extractor_test_multiple_rows',
            'sql' => [
                'CREATE TABLE soong_dbal_extractor_test_multiple_rows (id INTEGER NOT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, PRIMARY KEY(id))',
                "INSERT INTO soong_dbal_extractor_test_multiple_rows VALUES(1,'First Row','This row comes first.')",
                "INSERT INTO soong_dbal_extractor_test_multiple_rows VALUES(2,'Second Row','This row comes second.')",
                "INSERT INTO soong_dbal_extractor_test_multiple_rows VALUES(3,'Third Row','This row follows the second.')",
            ],
            'key_properties' => [
                'id' => [
                    'type' => 'integer',
                ]
            ],
            'data' => [
                [
                    'id' => 1,
                    'name' => 'First Row',
                    'description' => 'This row comes first.',
                ],
                [
                    'id' => 2,
                    'name' => 'Second Row',
                    'description' => 'This row comes second.',
                ],
                [
                    'id' => 3,
                    'name' => 'Third Row',
                    'description' => 'This row follows the second.',
                ],
            ],
        ];
        return $data;
    }

    /**
     * Test extraction of various types of values
     *
     * @return array
     */
    public function extractAllDataProvider() : array
    {
        $dataProvided = [];
        foreach ($this->data() as $dataSetName => $dataSet) {
            $dataProvided[$dataSetName] = [
                [
                    // @todo replace with mock
                    'data_record_class' => 'Soong\Data\Record',
                    'query' => 'SELECT * FROM ' . $dataSet['table_name'] .
                        ' ORDER BY ' . implode(',', array_keys($dataSet['key_properties']))
                        . ' ASC',
                    'key_properties' => $dataSet['key_properties'],
                ],
                $dataSet['data'],
            ];
        }
        return $dataProvided;
    }

    /**
     * @inheritdoc
     *
     * @dataProvider extractAllDataProvider
     */
    public function testExtractAll(array $configuration, array $expected)
    {
        parent::testExtractAll(array_merge($this->configuration, $configuration), $expected);
    }

    /**
     * @inheritdoc
     *
     * @dataProvider extractAllDataProvider
     */
    public function testCount(array $configuration, $expected)
    {
        parent::testCount(array_merge($this->configuration, $configuration), $expected);
    }

    /**
     * Test retrieval of property metadata.
     *
     * @return array
     */
    public function propertyDataProvider() : array
    {
        $data = [];
        foreach ($this->extractAllDataProvider() as $dataSetName => $dataSet) {
            $data[$dataSetName] = [$dataSet[0], [], $dataSet[0]['key_properties']];
        }
        return $data;
    }

    /**
     * @inheritdoc
     *
     * @dataProvider propertyDataProvider
     */
    public function testGetProperties(array $configuration, array $expectedProperties, array $expectedKeyProperties)
    {
        parent::testGetProperties(
            array_merge($this->configuration, $configuration),
            $expectedProperties,
            $expectedKeyProperties
        );
    }
}
