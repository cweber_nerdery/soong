<?php

namespace Soong\Tests\Extractor;

use Soong\Tests\Contracts\Extractor\CountableExtractorTestBase;

/**
 * Tests the \Soong\Extractor\Csv class.
 */
class CsvExtractorTest extends CountableExtractorTestBase
{

    /**
     * Specify the class we're testing.
     */
    protected function setUp() : void
    {
        $this->extractorClass = '\Soong\Extractor\Csv';
    }

    /**
     * Basic test data.
     *
     * @return array
     */
    private function data() : array
    {
        return [
            [
                'positive integer' => 11,
                'negative integer' => -23,
                'numeric string' => '563',
                'zero' => 0,
                'empty string' => '',
                'non-empty string' => 'a string',
                'float' => 1.2345,
                'null' => null,
            ],
        ];
    }

    /**
     * Key property configuration.
     *
     * @return array
     */
    private function keyProperties() : array
    {
        return [
            'positive integer' => [
                'type' => 'integer',
            ]
        ];
    }

    /**
     * Basic extractor configuration for testing.
     *
     * @return array
     */
    private function configuration() : array
    {
        return [
            // @todo replace with mock
            'record_class' => 'Soong\Data\Record',
            'csv_file_path' => __DIR__ . '/../test_data/test.csv',
            'key_properties' => $this->keyProperties(),
        ];
    }

    /**
     * Test extraction of various types of values
     *
     * @return array
     */
    public function extractAllDataProvider() : array
    {
        return [
            'set one' => [$this->configuration(), $this->data()],
        ];
    }

    /**
     * Test retrieval of property metadata.
     *
     * @return array
     */
    public function propertyDataProvider() : array
    {
        $properties = array_keys($this->data()[0]);
        return [
            'set one' => [$this->configuration(), $properties, $this->keyProperties()],
        ];
    }
}
