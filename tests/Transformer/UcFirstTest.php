<?php

namespace Soong\Tests\Transformer;

use Soong\Tests\Contracts\Transformer\TransFormerTestBase;

/**
 * Tests the \Soong\Transformer\UcFirst class.
 */
class UcFirstTest extends TransFormerTestBase
{

    /**
     * Specify the class we're testing.
     */
    protected function setUp() : void
    {
        $this->transformerClass = '\Soong\Transformer\UcFirst';
    }

    /**
     * Test capitalization of various values.
     *
     * @return array
     */
    public function transformerDataProvider() : array
    {
        return [
            'empty string' => [[], '', ''],
            'lowercase string' => [[], 'abc', 'Abc'],
            'uppercase string' => [[], 'ABC', 'ABC'],
            'non-alpha string' => [[], '12345', '12345'],
        ];
    }
}
