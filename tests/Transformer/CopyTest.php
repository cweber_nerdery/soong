<?php

namespace Soong\Tests\Transformer;

use Soong\Tests\Contracts\Transformer\TransformerTestBase;

/**
 * Tests the \Soong\Transformer\Copy class.
 */
class CopyTest extends TransformerTestBase
{

    /**
     * Specify the class we're testing.
     */
    protected function setUp() : void
    {
        $this->transformerClass = '\Soong\Transformer\Copy';
    }

    /**
     * Test copying of various types of values
     *
     * @return array
     */
    public function transformerDataProvider() : array
    {
        return [
            'positive integer' => [[], 1, 1],
            'negative integer' => [[], -1, -1],
            'numeric string' => [[], '1', '1'],
            'zero' => [[], 0, 0],
            'empty string' => [[], '', ''],
            'non-empty string' => [[], 'a string', 'a string'],
            'float' => [[], 1.2345, 1.2345],
            'null' => [[], null, null],
            'empty array' => [[], [], []],
            'non-empty array' => [[], [1, 2, 'five'], [1, 2, 'five']],
            'keyed array' => [[], ['a' => 'b', 'c' => 'd'], ['a' => 'b', 'c' => 'd']],
            'object' => [[], new \stdClass(), new \stdClass()],
            'true' => [[], true, true],
            'false' => [[], false, false],
        ];
    }
}
