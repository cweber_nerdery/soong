<?php

namespace Soong\Tests\Transformer;

use Soong\Tests\Contracts\Transformer\TransFormerTestBase;

/**
 * Tests the \Soong\Transformer\Increment class.
 */
class IncrementTest extends TransFormerTestBase
{

    /**
     * Specify the class we're testing.
     */
    protected function setUp() : void
    {
        $this->transformerClass = '\Soong\Transformer\Increment';
    }

    /**
     * Test incrementing of various types of values
     *
     * @return array
     */
    public function transformerDataProvider() : array
    {
        return [
            'positive integer' => [[], 1, 2],
            'negative integer' => [[], -1, 0],
            'numeric string' => [[], '1', 2],
            'zero' => [[], 0, 1],
            'float' => [[], 1.2345, 2.2345],
            'null' => [[], null, 1],
            'true' => [[], true, 2],
            'false' => [[], false, 1],
        ];
    }
}
