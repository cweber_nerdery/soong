<?php

namespace Soong\Tests\Transformer;

use Soong\Contracts\Data\DataProperty;
use Soong\Contracts\KeyMap\KeyMap;
use Soong\Tests\Contracts\Transformer\TransFormerTestBase;

/**
 * Tests the \Soong\Transformer\KeyMapLookup class.
 */
class KeyMapLookupTest extends TransFormerTestBase
{

    /**
     * Specify the class we're testing.
     */
    protected function setUp() : void
    {
        $this->transformerClass = '\Soong\Transformer\KeyMapLookup';
    }

    /**
     * Test looking up various types of values.
     *
     * @return array
     */
    public function transformerDataProvider() : array
    {
        $configuration['key_map']['task_id'] = 'dummy_task';
        // We will mock a key map instance which returns the last value of
        // each row here when passed the middle value. Note the difference
        // between 'mapped to null' and 'unmatched' is that 'mapped to null' is
        // an explicit mapping to null in the keymap, while the 'unmatched'
        // mapping is not added to the keymap at all.
        $providedData = [
            'integer' => [$configuration, 1, 2],
            'string' => [$configuration, 'foo', 'bar'],
            'mapped to null' => [$configuration, 'baz', null],
            'unmatched' => [$configuration, 'blah', null],
        ];
        $returnMap = [];
        foreach ($providedData as $description => $dataInstance) {
            if ($description !== 'unmatched') {
                $property = $this->createMock(DataProperty::class);
                $property->method('getValue')->willReturn($dataInstance[1]);
                $returnMap[] = [$property, $dataInstance[2]];
            }
        }
        $keyMap = $this->createMock(KeyMap::class);
        $keyMap->method('lookupLoadedKey')->will($this->returnValueMap($returnMap));

        // @todo Because this transformer depends on a static method with an
        // explicit concrete class, we can't properly test it until
        // https://gitlab.com/soongetl/soong/issues/58 is done.
        return [];
//        return $providedData;
    }
}
