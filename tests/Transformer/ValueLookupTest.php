<?php

namespace Soong\Tests\Transformer;

use Soong\Tests\Contracts\Transformer\TransFormerTestBase;

/**
 * Tests the \Soong\Transformer\ValueLookup class.
 */
class ValueLookupTest extends TransFormerTestBase
{

    /**
     * Specify the class we're testing.
     */
    protected function setUp() : void
    {
        $this->transformerClass = '\Soong\Transformer\ValueLookup';
    }

    /**
     * Test looking up various types of values.
     *
     * @return array
     */
    public function transformerDataProvider() : array
    {
        $lookupTable = [
            'lookup_table' => [
                -1 => -5,
                '1' => 'one',
                '0' => 'zero',
                '' => 'empty string',
                'a string' => 'b string',
                '1.2345' => 6.789,
            ]
        ];
        return [
            'negative integer' => [$lookupTable, -1, -5],
            'numeric string' => [$lookupTable, '1', 'one'],
            'zero' => [$lookupTable, 0, 'zero'],
            'empty string' => [$lookupTable, '', 'empty string'],
            'non-empty string' => [$lookupTable, 'a string', 'b string'],
            'float' => [$lookupTable, '1.2345', 6.789],
        ];
    }
}
