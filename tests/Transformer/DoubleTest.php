<?php

namespace Soong\Tests\Transformer;

use Soong\Tests\Contracts\Transformer\TransFormerTestBase;

/**
 * Tests the \Soong\Transformer\Double class.
 */
class DoubleTest extends TransFormerTestBase
{

    /**
     * Specify the class we're testing.
     */
    protected function setUp() : void
    {
        $this->transformerClass = '\Soong\Transformer\Double';
    }

    /**
     * Test doubling of various types of values
     *
     * @return array
     */
    public function transformerDataProvider() : array
    {
        return [
            'positive integer' => [[], 1, 2],
            'negative integer' => [[], -1, -2],
            'numeric string' => [[], '1', 2],
            'zero' => [[], 0, 0],
            'float' => [[], 1.2345, 2.469],
            'null' => [[], null, 0],
            'true' => [[], true, 2],
            'false' => [[], false, 0],
        ];
    }
}
