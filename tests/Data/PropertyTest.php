<?php

namespace Soong\Tests\Data;

use Soong\Tests\Contracts\Data\DataPropertyTestBase;

/**
 * Tests the \Soong\Data\Property class.
 */
class PropertyTest extends DataPropertyTestBase
{

    /**
     * Specify the class we're testing.
     */
    protected function setUp() : void
    {
        $this->propertyClass = '\Soong\Data\Property';
    }
}
