<?php
declare(strict_types=1);

namespace Soong\Extractor;

use Soong\Data\Record;

/**
 * Extractor for in-memory arrays.
 */
class ArrayExtractor extends CountableExtractorBase
{

    /**
     * @inheritdoc
     */
    public function extractAll() : iterable
    {
        foreach ($this->configuration['data'] as $data) {
            // @todo: Inject DataRecord implementation.
            yield Record::create($data);
        }
    }

    /**
     * @inheritdoc
     */
    public function getProperties(): array
    {
        return array_keys($this->configuration['data'][0]);
    }

    /**
     * @inheritdoc
     */
    public function count()
    {
        return count($this->configuration['data']);
    }
}
