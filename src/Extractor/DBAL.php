<?php
declare(strict_types=1);

namespace Soong\Extractor;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\FetchMode;
use Soong\Contracts\Data\DataRecord;

/**
 * Extractor for DBAL SQL queries.
 */
class DBAL extends CountableExtractorBase
{

    use \Soong\DBAL\DBAL;

    /**
     * @inheritdoc
     */
    public function extractAll() : iterable
    {
        try {
            // @todo: don't accept raw SQL from configuration
            /** @var \Doctrine\DBAL\Driver\Statement $statement */
            $statement = $this->connection()->executeQuery($this->configuration['query']);
            while ($row = $statement->fetch(FetchMode::ASSOCIATIVE)) {
                /** @var DataRecord $dataRecordClass */
                $dataRecordClass = $this->configuration['data_record_class'];
                yield $dataRecordClass::create($row);
            }
        } catch (DBALException $e) {
            // @todo
        }
        $this->connection->close();
        unset($this->connection);
    }

    /**
     * @inheritdoc
     */
    public function getProperties(): array
    {
        // @todo: Identify properties from the query.
        return parent::getProperties();
    }

    // @todo implement count() to use SQL COUNT().
}
