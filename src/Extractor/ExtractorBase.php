<?php
declare(strict_types=1);

namespace Soong\Extractor;

use Soong\Contracts\Extractor\Extractor;

/**
 * Common implementation details for extractors.
 */
abstract class ExtractorBase implements Extractor
{

    /**
     * Configuration for the extractor.
     *
     * @var array $configuration
     *   Configuration values keyed by configuration name.
     */
    protected $configuration = [];

    /**
     * Save the configuration.
     *
     * @param array $configuration
     *   Configuration values keyed by configuration name.
     */
    protected function __construct(array $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @inheritdoc
     */
    public static function create(array $configuration): Extractor
    {
        return new static($configuration);
    }

    /**
     * @inheritdoc
     */
    public function extractFiltered() : iterable
    {
        return $this->extractAll();
    }

    /**
     * @inheritdoc
     */
    public function getProperties(): array
    {
        return $this->configuration[self::CONFIGURATION_PROPERTIES] ?? [];
    }

    /**
     * @inheritdoc
     */
    public function getKeyProperties(): array
    {
        return $this->configuration[self::CONFIGURATION_KEY_PROPERTIES] ?? [];
    }
}
