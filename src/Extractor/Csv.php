<?php
declare(strict_types=1);

namespace Soong\Extractor;

use League\Csv\Reader;
use Soong\Contracts\Data\DataRecord;

/**
 * CSV extractor based on The League CSV library.
 */
class Csv extends CountableExtractorBase
{

    /**
     * @inheritdoc
     */
    public function extractAll(): iterable
    {
        $csv = $this->loadCsv();
        /** @var DataRecord $recordClass */
        $recordClass = $this->configuration['record_class'];
        foreach ($csv->getRecords() as $record) {
            yield $recordClass::create($record);
        }
    }

    /**
     * @inheritdoc
     */
    public function getProperties(): array
    {
        return $this->loadCsv()->getHeader();
    }

    /**
     * @internal
     *
     * Obtain a CSV Reader object for the configured file path.
     *
     * @return Reader
     *   A Reader object representing the CSV file we're extracting.
     */
    protected function loadCsv() : Reader
    {
        $csv = Reader::createFromPath($this->configuration['csv_file_path'], 'r');
        // @todo configure the header
        $csv->setHeaderOffset(0);
        return $csv;
    }

    /**
     * @inheritdoc
     */
    public function count()
    {
        $csv = $this->loadCsv();
        return $csv->count();
    }
}
