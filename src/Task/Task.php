<?php
declare(strict_types=1);

namespace Soong\Task;

use Soong\Contracts\Task\Task as TaskInterface;

/**
 * Basic base class for migration tasks.
 */
class Task implements TaskInterface
{
    /**
     * @internal
     *
     * Configuration values keyed by configuration name.
     *
     * @var array $configuration
     */
    protected $configuration = [];

    /**
     * @internal
     *
     * All known tasks, keyed by id.
     *
     * @var Task[] $tasks
     */
    static protected $tasks = [];

    /**
     * Save the configuration.
     *
     * @param array $configuration
     *   Configuration values keyed by configuration name.
     */
    protected function __construct(array $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @inheritdoc
     */
    public static function create(array $configuration): TaskInterface
    {
        return new static($configuration);
    }

    /**
     * @inheritdoc
     */
    public function getConfiguration(): array
    {
        return $this->configuration;
    }

    /**
     * @inheritdoc
     */
    public function execute(string $operation, array $options = []) : void
    {
        if (method_exists($this, $operation)) {
            $this->$operation($options);
        }
    }

    /**
     * @inheritdoc
     */
    public function isCompleted(): bool
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    public static function addTask(string $id, array $configuration) : void
    {
        static::$tasks[$id] = new static($configuration);
    }

    /**
     * @inheritdoc
     */
    public static function getTask(string $id): TaskInterface
    {
        return static::$tasks[$id];
    }

    /**
     * @inheritdoc
     */
    public static function getAllTasks(): array
    {
        return static::$tasks;
    }
}
