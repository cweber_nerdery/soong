<?php
declare(strict_types=1);

namespace Soong\Task;

use Soong\Contracts\Extractor\Extractor;
use Soong\Contracts\KeyMap\KeyMap;
use Soong\Contracts\Loader\Loader;
use Soong\Contracts\Task\EtlTask as EtlTaskInterface;
use Soong\Contracts\Transformer\Transformer;

/**
 * Implementation of operations for a full ETL process.
 */
class EtlTask extends Task implements EtlTaskInterface
{

    /**
     * @inheritdoc
     */
    public function getExtractor(): ?Extractor
    {
        $taskConfiguration = $this->configuration;
        if (empty($taskConfiguration['extract'])) {
            return null;
        }
        /** @var \Soong\Contracts\Extractor\Extractor $extractorClass */
        $extractorClass = $taskConfiguration['extract']['class'];
        $extractor = $extractorClass::create($taskConfiguration['extract']['configuration']);
        return $extractor;
    }

    /**
     * @inheritdoc
     */
    public function getLoader(): ?Loader
    {
        $taskConfiguration = $this->configuration;
        if (empty($taskConfiguration['load'])) {
            return null;
        }
        /** @var \Soong\Contracts\Loader\Loader $loaderClass */
        $loaderClass = $taskConfiguration['load']['class'];
        $loader = $loaderClass::create($taskConfiguration['load']['configuration']);
        return $loader;
    }

    /**
     * @inheritdoc
     */
    public function getKeyMap() : ?KeyMap
    {
        $taskConfiguration = $this->configuration;
        if (empty($taskConfiguration['extract'])) {
            return null;
        }
        /** @var \Soong\Contracts\Extractor\Extractor $extractor */
        $extractor = $this->getExtractor();
        $extractorKeys = $extractor->getKeyProperties();

        /** @var \Soong\Contracts\Loader\Loader $loader */
        $loader = $this->getLoader();
        $loaderKeys = $loader->getKeyProperties();

        $keyMapConfiguration = $taskConfiguration['key_map']['configuration'] ?? [];
        if (empty($keyMapConfiguration)) {
            return null;
        }
        $keyMapConfiguration = array_merge(
            [KeyMap::CONFIGURATION_EXTRACTOR_KEYS => $extractorKeys],
            $keyMapConfiguration
        );
        $keyMapConfiguration = array_merge(
            [KeyMap::CONFIGURATION_LOADER_KEYS => $loaderKeys],
            $keyMapConfiguration
        );
        /** @var \Soong\Contracts\KeyMap\KeyMap $keyMapClass */
        $keyMapClass = $taskConfiguration['key_map']['class'];
        $keyMap = $keyMapClass::create($keyMapConfiguration);
        return $keyMap;
    }

    /**
     * @internal
     *
     * Create a transformer instance of a given class and configuration.
     *
     * @param array $transformerStuff
     *
     * @return Transformer
     */
    protected function getTransformer(array $transformerStuff) : Transformer
    {
        $transformerConfiguration = $transformerStuff['configuration'] ?? [];
        /** @var \Soong\Contracts\Transformer\Transformer $transformerClass */
        $transformerClass = $transformerStuff['class'];
        $transformer = $transformerClass::create($transformerConfiguration);
        return $transformer;
    }

    /**
     * Perform an ETL migration operation.
     *
     * @param array $options
     */
    public function migrate(array $options)
    {
        $taskConfiguration = $this->configuration;
        if (empty($taskConfiguration['extract'])) {
            return;
        }

        $extractor = $this->getExtractor();
        $loader = $this->getLoader();
        $keyMap = $this->getKeyMap();
        /** @var \Soong\Contracts\Data\DataRecord $recordClass */
        $recordClass = $taskConfiguration['record_class'];

        /** @var \Soong\Contracts\Data\DataRecord $data */
        foreach ($extractor->extractFiltered() as $data) {
            $resultData = $recordClass::create();
            if (isset($taskConfiguration['transform'])) {
                foreach ($taskConfiguration['transform'] as $property => $transformerList) {
                    // Shortcut for directly mapping properties.
                    if (is_string($transformerList)) {
                        $source = $transformerList;
                        $transformerList = [
                            [
                                'class' => 'Soong\Transformer\Copy',
                                'source' => $source,
                            ],
                        ];
                    }
                    foreach ($transformerList as $transformerStuff) {
                        $transformer = $this->getTransformer($transformerStuff);
                        $currentData = isset($transformerStuff['source'])
                          ? $data->getProperty($transformerStuff['source'])
                          : $resultData->getProperty($property);
                        $resultData->setProperty(
                            $property,
                            $transformer->transform($currentData)
                        );
                    }
                }
            }
            $loader->load($resultData);
            // @todo Handle multi-column keys.
            $extractedKey = $data->getProperty(array_keys($extractor->getKeyProperties())[0])->getValue();
            $loadedKey = $resultData->getProperty(array_keys($loader->getKeyProperties())[0])->getValue();
            if (isset($keyMap)) {
                $keyMap->saveKeyMap([$extractedKey], [$loadedKey]);
            }
        }
    }

    /**
     * Rollback an ETL migration operation.
     *
     * @param array $options
     */
    public function rollback(array $options)
    {
        $taskConfiguration = $this->configuration;
        if (empty($taskConfiguration['extract'])) {
            return;
        }

        $loader = $this->getLoader();
        $keyMap = $this->getKeyMap();

        foreach ($keyMap->iterate() as $extractedKey) {
            $loadedKey = $keyMap->lookupLoadedKey($extractedKey);
            if (!empty($loadedKey)) {
                $loader->delete($loadedKey);
                $keyMap->delete($extractedKey);
            }
        }
    }
}
