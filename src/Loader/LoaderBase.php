<?php
declare(strict_types=1);

namespace Soong\Loader;

use Soong\Contracts\Loader\Loader;

/**
 * Common implementation details for loaders.
 */
abstract class LoaderBase implements Loader
{

    /**
     * Configuration for the loader.
     *
     * @var array $configuration
     *   Configuration values keyed by configuration name.
     */
    protected $configuration = [];

    /**
     * Save the configuration.
     *
     * @param array $configuration
     *   Configuration values keyed by configuration name.
     */
    protected function __construct(array $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @inheritdoc
     */
    public static function create(array $configuration): Loader
    {
        return new static($configuration);
    }
}
