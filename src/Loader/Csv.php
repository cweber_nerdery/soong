<?php
declare(strict_types=1);

namespace Soong\Loader;

use Soong\Contracts\Data\DataRecord;
use Soong\Data\Property;

/**
 * Real dumb demo of a simple loader.
 *
 * @todo: Use league/csv
 */
class Csv extends LoaderBase
{

    /**
     * @internal
     *
     * Counter of records loaded, to use as a unique destination ID.
     *
     * @var int $counter
     */
    protected $counter = 0;

    /**
     * @inheritdoc
     */
    public function load(DataRecord $data) : void
    {
        // @todo: Don't use concrete Property class.
        $data->setProperty(array_keys($this->configuration['key_properties'])[0], Property::create($this->counter++));
        $properties = $data->toArray();
        if (count($properties) > 1) {
            if ($this->counter == 1) {
                print implode(',', $this->configuration['properties']) . "\n";
            }
            print implode(',', $properties) . "\n";
        }
    }

    /**
     * @inheritdoc
     */
    public function getProperties(): array
    {
        return $this->configuration['properties'] ?? [];
    }

    /**
     * @inheritdoc
     */
    public function getKeyProperties(): array
    {
        return $this->configuration['key_properties'] ?? [];
    }

    /**
     * @inheritdoc
     */
    public function delete(array $key) : void
    {
        // @todo not supported
    }
}
