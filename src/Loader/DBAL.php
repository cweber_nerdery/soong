<?php
declare(strict_types=1);

namespace Soong\Loader;

use Doctrine\DBAL\DBALException;
use Soong\Contracts\Data\DataRecord;
use Soong\Data\Property;

/**
 * Loader for DBAL SQL tables.
 */
class DBAL extends LoaderBase
{

    use \Soong\DBAL\DBAL;

    /**
     * @inheritdoc
     */
    public function load(DataRecord $data) : void
    {
        try {
            $this->connection()->insert(
                $this->configuration['table'],
                $data->toArray()
            );
            $id = $this->connection()->lastInsertId();
            if ($id) {
                $keyKeys = array_keys($this->getKeyProperties());
                // @todo Inject DataProperty instance.
                $data->setProperty(reset($keyKeys), Property::create($id));
            }
        } catch (DBALException $e) {
            print $e->getMessage();
        }
    }

    /**
     * @inheritdoc
     */
    public function getProperties(): array
    {
        return $this->configuration['properties'] ?? [];
    }

    /**
     * @inheritdoc
     */
    public function getKeyProperties(): array
    {
        return $this->configuration['key_properties'] ?? [];
    }

    /**
     * @inheritdoc
     */
    public function delete(array $key) : void
    {
        $queryBuilder = $this->connection()->createQueryBuilder();
        $queryBuilder->delete($this->configuration['table']);
        $counter = 0;
        foreach ($key as $name => $value) {
            $queryBuilder->andWhere("$name = ?");
            $queryBuilder->setParameter($counter, $value);
            $counter++;
        }
        $queryBuilder->execute();
    }
}
