<?php
declare(strict_types=1);

namespace Soong\Data;

use Soong\Contracts\Data\DataProperty;

/**
 * Immutable data property wrapper implementation.
 */
class Property implements DataProperty
{

    /**
     * @internal
     *
     * The value being wrapped.
     *
     * @var mixed $value
     */
    protected $value;

    /**
     * @internal
     *
     * Store the value.
     *
     * @param mixed $value
     */
    protected function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @inheritdoc
     */
    public static function create($value): DataProperty
    {
        return new static($value);
    }

    /**
     * @inheritdoc
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @inheritdoc
     */
    public function isEmpty() : bool
    {
        return is_null($this->value);
    }
}
