<?php
declare(strict_types=1);

namespace Soong\Data;

use Soong\Contracts\Data\DataProperty;
use Soong\Contracts\Data\DataRecord;

/**
 * Basic implementation of data records as arrays.
 */
class Record implements DataRecord
{

    /**
     * @internal
     *
     * Array of data properties, keyed by property name.
     *
     * @var DataProperty $data[]
     */
    protected $data = [];

    /**
     * @internal
     *
     * Protected constructor, to prevent direct creation via 'new'.
     */
    protected function __construct()
    {
    }

    /**
     * @inheritdoc
     */
    public static function create(array $data = []) : DataRecord
    {
        $record = new Record();
        foreach ($data as $propertyName => $propertyValue) {
            // @todo Inject DataProperty implementation.
            $record->setProperty($propertyName, Property::create($propertyValue));
        }
        return $record;
    }

    /**
     * @inheritdoc
     */
    public function setProperty(string $propertyName, DataProperty $propertyValue) : void
    {
        $this->data[$propertyName] = $propertyValue;
    }

    /**
     * @inheritdoc
     */
    public function getProperty(string $propertyName) : DataProperty
    {
        return isset($this->data[$propertyName]) ? $this->data[$propertyName] : $this->nullProperty();
    }

    /**
     * @internal
     *
     * Provide a property with a null value.
     *
     * @return DataProperty
     *   A property object containing a null value.
     */
    protected function nullProperty() : DataProperty
    {
        // @todo Inject property implementation.
        return Property::create(null);
    }

    /**
     * @inheritdoc
     */
    public function toArray() : array
    {
        $result = [];
        /** @var DataProperty $propertyValue */
        foreach ($this->data as $propertyName => $propertyValue) {
            if (!is_null($propertyValue)) {
                $result[$propertyName] = $propertyValue->getValue();
            }
        }
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function propertyExists(string $propertyName): bool
    {
        return isset($this->data[$propertyName]);
    }
}
