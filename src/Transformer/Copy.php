<?php
declare(strict_types=1);

namespace Soong\Transformer;

use Soong\Contracts\Data\DataProperty;

/**
 * Transformer to simply copy extracted data to the destination.
 */
class Copy extends TransformerBase
{

    /**
     * @inheritdoc
     */
    public function transform(DataProperty $data) : DataProperty
    {
        return $data;
    }
}
