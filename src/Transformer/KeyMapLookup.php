<?php
declare(strict_types=1);

namespace Soong\Transformer;

use Soong\Data\Property;
use Soong\Contracts\Data\DataProperty;
use Soong\Task\EtlTask;

/**
 * Transformer accepting a unique key from the extracted data and looking up the
 * key of any data loaded from it.
 *
 * Configuration:
 *   key_map:
 *     task_id: Unique identifier of the EtlTask which migrated the data.
 */
class KeyMapLookup extends TransformerBase
{

    /**
     * @inheritdoc
     */
    public function transform(DataProperty $data) : DataProperty
    {
        if (!$data->isEmpty()) {
            $keyMapConfig = $this->configuration['key_map'];
            /** @var \Soong\Contracts\Task\EtlTask $task */
            $task = EtlTask::getTask($keyMapConfig['task_id']);
            // @todo: Allow multiple key maps.
            $keyMap = $task->getKeyMap();
            $loadedKey = $keyMap->lookupLoadedKey([$data->getValue()]);
            if (!empty($loadedKey)) {
                // @todo: Handle multi-value keys properly.
                // @todo Don't use concrete class
                return Property::create(reset($loadedKey));
            }
            // @todo: Support creation of stubs when nothing found.
        }
        return Property::create(null);
    }
}
