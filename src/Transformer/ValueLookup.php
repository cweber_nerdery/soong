<?php
declare(strict_types=1);

namespace Soong\Transformer;

use Soong\Contracts\Data\DataProperty;
use Soong\Data\Property;

/**
 * Transformer to lookup a value to be returned based on an input value.
 */
class ValueLookup extends TransformerBase
{

    /**
     * @inheritdoc
     */
    public function transform(DataProperty $data) : DataProperty
    {
        return Property::create($this->configuration['lookup_table'][$data->getValue()]);
    }
}
