<?php
declare(strict_types=1);

namespace Soong\Transformer;

use Soong\Contracts\Data\DataProperty;
use Soong\Data\Property;

/**
 * Transformer to multiply the extracted data value by 2.
 */
class Double extends TransformerBase
{

    /**
     * @inheritdoc
     */
    public function transform(DataProperty $data) : DataProperty
    {
        // @todo Don't use concrete class
        return Property::create(2 * $data->getValue());
    }
}
