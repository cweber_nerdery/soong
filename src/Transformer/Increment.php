<?php
declare(strict_types=1);

namespace Soong\Transformer;

use Soong\Contracts\Data\DataProperty;
use Soong\Data\Property;

/**
 * Transformer to add 1 to the extracted data.
 */
class Increment extends TransformerBase
{

    /**
     * @inheritdoc
     */
    public function transform(DataProperty $data) : DataProperty
    {
        // @todo Don't use concrete class
        return Property::create($data->getValue() + 1);
    }
}
