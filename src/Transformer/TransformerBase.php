<?php
declare(strict_types=1);

namespace Soong\Transformer;

use Soong\Contracts\Transformer\Transformer;

/**
 * Loaders take a DataRecord and put it... somewhere.
 */
abstract class TransformerBase implements Transformer
{

    /**
     * Configuration for this transformer.
     *
     * @var array $configuration
     */
    protected $configuration = [];

    /**
     * Construct a transformer.
     *
     * @param array $configuration
     *   Array of configuration options keyed by option name.
     */
    protected function __construct(array $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @inheritdoc
     */
    public static function create(array $configuration): Transformer
    {
        return new static($configuration);
    }
}
