<?php
declare(strict_types=1);

namespace Soong\Transformer;

use Soong\Data\Property;
use Soong\Contracts\Data\DataProperty;

/**
 * Transformer to uppercase the first letter of the extracted data.
 */
class UcFirst extends TransformerBase
{

    /**
     * @inheritdoc
     */
    public function transform(DataProperty $data) : DataProperty
    {
        // @todo Don't use concrete class
        return Property::create(ucfirst($data->getValue()));
    }
}
