<?php
declare(strict_types=1);

namespace Soong\Contracts\Data;

/**
 * Collection of named data properties.
 */
interface DataRecord
{

    /**
     * Create a record populated with a set of named data properties.
     *
     * @param array $data
     *   Associative array of property values, keyed by property name.
     *
     * @return DataRecord
     *   A newly-created data record.
     */
    public static function create(array $data = []) : DataRecord;

    /**
     * Fetch the list of named properties as an associative array.
     *
     * @return array
     *   Associative array of property values, keyed by property name.
     */
    public function toArray() : array;

    /**
     * Set a property from an existing DataProperty object.
     *
     * @param string $propertyName
     *   Name of the property to set.
     * @param DataProperty $propertyValue
     *   Property value to set.
     */
    public function setProperty(string $propertyName, DataProperty $propertyValue) : void;

    /**
     * Retrieve a property value as a DataProperty.
     *
     * @param string $propertyName
     *   Name of the property to get.
     *
     * @return DataProperty
     *   Value of the property.
     */
    public function getProperty(string $propertyName) : DataProperty;

    /**
     * Does the named property exist in this record?
     *
     * @param string $propertyName
     *   Name of the property to get.
     *
     * @return bool
     *   TRUE if the property exists (even if its value is NULL), FALSE otherwise.
     */
    public function propertyExists(string $propertyName) : bool;
}
