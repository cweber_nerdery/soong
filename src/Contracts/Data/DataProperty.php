<?php
declare(strict_types=1);

namespace Soong\Contracts\Data;

/**
 * Immutable data property.
 */
interface DataProperty
{

    /**
     * Create a property instance for the given value.
     *
     * @param mixed $value
     *
     * @return DataProperty
     */
    public static function create($value) : DataProperty;

    /**
     * Return the property value.
     *
     * @return mixed
     */
    public function getValue();

    /**
     * TRUE if the property has a NULL value.
     *
     * @return bool
     */
    public function isEmpty() : bool;
}
