<?php
declare(strict_types=1);

namespace Soong\Contracts\Task;

/**
 * Interface for tasks implementing operations as part of a pipeline.
 */
interface Task
{

    /**
     * Create a task instance using the provided configuration.
     *
     * @param array $configuration
     *
     * @return Task
     */
    public static function create(array $configuration) : Task;

    /**
     * Execute the named operation for the task.
     *
     * @param string $operation
     *   Name of the operation, which is a method on the class.
     * @param array $options
     *   List of options affecting execution of the operation.
     */
    public function execute(string $operation, array $options = []) : void;

    /**
     * Return the task's configuration as an array.
     *
     * @return array
     */
    public function getConfiguration() : array;

    /**
     * Indicate whether dependent tasks may proceed.
     *
     * @return bool
     *   TRUE if this task has no outstanding records to process, else FALSE.
     */
    public function isCompleted(): bool;

    /**
     * Retrieve the specified task.
     *
     * @param string $id
     *   ID of the task to retrieve.
     *
     * @return Task
     *   The specified task.
     */
    public static function getTask(string $id) : Task;

    /**
     * Retrieve a list of all tasks.
     *
     * @return Task[]
     *   List of tasks, keyed by ID.
     */
    public static function getAllTasks() : array;

    /**
     * Create a task object with a given id from provided configuration.
     *
     * @param string $id
     *   ID of the task to add.
     * @param array $configuration
     *   Configuration of the task to add.
     *
     * @return void
     */
    public static function addTask(string $id, array $configuration) : void;
}
