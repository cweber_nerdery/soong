<?php
declare(strict_types=1);

namespace Soong\Contracts\Transformer;

use Soong\Contracts\Data\DataProperty;

/**
 * Accept a data property and turn it into another data property.
 */
interface Transformer
{

    /**
     * Create a transformer instance using the provided configuration.
     *
     * @param array $configuration
     *   Configuration of the transformer.
     *
     * @return Transformer
     *   A newly-created transformer.
     */
    public static function create(array $configuration) : Transformer;

    /**
     * Accept a data property and turn it into another data property.
     *
     * @param DataProperty $data
     *   Property containing data to be transformed.
     *
     * @return DataProperty
     *   The transformed data.
     */
    public function transform(DataProperty $data) : DataProperty;
}
