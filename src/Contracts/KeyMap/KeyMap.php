<?php
declare(strict_types=1);

namespace Soong\Contracts\KeyMap;

/**
 * Represents the mapping of extracted keys to loaded keys.
 */
interface KeyMap extends \Countable
{

    /**
     * Configuration key for specifying the key names for the extractor.
     */
    const CONFIGURATION_EXTRACTOR_KEYS = 'extractor_keys';

    /**
     * Configuration key for specifying the key names for the loader.
     */
    const CONFIGURATION_LOADER_KEYS = 'loader_keys';

    /**
     * Create a keymap instance using the provided configuration.
     *
     * @param array $configuration
     *
     * @return KeyMap
     */
    public static function create(array $configuration) : KeyMap;

    /**
     * Persist the mapping of an extracted key to the corresponding loaded key.
     *
     * @param array $extractedKey
     *   Extracted key values, keyed by key names.
     *
     * @param array $loadedKey
     *   Loaded key values, keyed by key names.
     */
    public function saveKeyMap(array $extractedKey, array $loadedKey) : void;

    /**
     * Retrieve the loaded key corresponding to a given extracted key.
     *
     * @param array $extractedKey
     *   Extracted key values, keyed by key names.
     *
     * @return array
     *   Loaded key values, keyed by key names.
     */
    public function lookupLoadedKey(array $extractedKey) : array;

    /**
     * Retrieve any extracted keys mapped to a given loaded key.
     *
     * Note that multiple extracted keys may map to one loaded key - while
     * lookupLoadedKey returns a single key array, lookupExtractedKeys returns
     * an array of key arrays.
     *
     * @param array $loadedKey
     *   Loaded key values, keyed by key names.
     *
     * @return array[]
     *   Array of extracted keys, each of which is keyed by key names.
     */
    public function lookupExtractedKeys(array $loadedKey) : array;

    /**
     * Remove the mapping for a given extracted key from the map.
     *
     * @param array $extractedKey
     *   Extracted key values, keyed by key names.
     */
    public function delete(array $extractedKey) : void;

    /**
     * Iterate over the key map, generating the keys.
     *
     * @todo: Make the class \Iterable instead?
     *
     * @return iterable
     */
    public function iterate() : iterable;
}
