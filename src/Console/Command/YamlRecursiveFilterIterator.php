<?php
declare(strict_types=1);

namespace Soong\Console\Command;

/**
 * Gather all YAML files in a directory.
 */
class YamlRecursiveFilterIterator extends \RecursiveFilterIterator
{

    /**
     * @inheritdoc
     */
    public function accept()
    {
        $file = parent::current();
        if ($file->isDir()) {
            return true;
        }
        $name = $file->getFilename();
        return (substr($name, -4) == '.yml' || substr($name, -5) == '.yaml');
    }
}
