<?php
declare(strict_types=1);

namespace Soong\Console\Command;

use Soong\Contracts\Task\Task;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Yaml\Yaml;

/**
 * Base class for all Soong console commands.
 */
class EtlCommand extends Command
{

    /**
     * Configure the "tasks" command argument for one or more values.
     *
     * @param bool $required
     *   TRUE if an explicit task is required, FALSE otherwise.
     *
     * @return InputArgument
     *   The configured command argument.
     */
    protected function tasksArgument(bool $required = true) : InputArgument
    {
        $required = $required ? InputArgument::REQUIRED : InputArgument::OPTIONAL;
        return new InputArgument(
            'tasks',
            InputArgument::IS_ARRAY | $required,
            'List of task IDs to process'
        );
    }

    /**
     * Configure the "directory" option to be required with one or more values.
     *
     * @return InputOption
     *   The configured command option.
     */
    protected function directoryOption() : InputOption
    {
        return new InputOption(
            'directory',
            null,
            InputOption::VALUE_IS_ARRAY | InputOption::VALUE_REQUIRED,
            'List of directories containing tasks to migrate',
            ['config']
        );
    }

    /**
     * Obtain all task configuration contained in the specified directories.
     *
     * @param array $directoryNames
     *   List of directories containing task configuration.
     */
    protected function loadConfiguration(array $directoryNames) : void
    {
        foreach ($directoryNames as $directoryName) {
            $directory = new \RecursiveDirectoryIterator(
                $directoryName,
                \FilesystemIterator::CURRENT_AS_FILEINFO | \FilesystemIterator::SKIP_DOTS
            );
            $filter = new YamlRecursiveFilterIterator($directory);
            $allFiles = new \RecursiveIteratorIterator(
                $filter,
                \RecursiveIteratorIterator::SELF_FIRST
            );
            foreach ($allFiles as $file) {
                if (!$file->isDir()) {
                    $configuration = Yaml::parse(file_get_contents($file->getPathname()));
                    foreach ($configuration as $id => $taskConfiguration) {
                        /** @var Task $taskClass */
                        $taskClass = $taskConfiguration['class'];
                        $taskClass::addTask($id, $taskConfiguration['configuration']);
                    }
                }
            }
        }
    }
}
