# Welcome to Soong

Soong is a framework for building robust [Extract-Transform-Load (ETL)][d54f729a] applications for performing data migration. It is designed to be record-oriented and configuration-driven - many applications will require little or no custom PHP code, and tools can easily customize (or create) data migration processes implemented by Soong.

## Overview

The core of Soong is a set of interfaces defining a highly decoupled architecture. Each component (other than `Task`) may be used standalone, but is primarily designed to be part of an Extract→Transform→Load pipeline for processing sets of data one record at a time. At this stage of development, the main repository also includes basic implementations sufficient to build simple ETL applications and examples - before V1.0.0 is released, most if not all implementations will be moved to separate repositories.

## Installation

With [Composer installed globally][f739d78f] on your system, at the command line enter:

  [f739d78f]: https://getcomposer.org/doc/00-intro.md#installation-linux-unix-macos "Composer installation"

```sh
composer require soong/soong
```

Soong comes with some default Symfony Console commands to run your task configurations, invoked from `bin/soong`, e.g.:

```sh
bin/soong migrate beeraccounts
```

To autoload Soong classes in your custom CLI or web application:

```php
require_once 'vendor/autoload.php';
```

  [d54f729a]: https://en.wikipedia.org/wiki/Extract,_transform,_load "ETL"
