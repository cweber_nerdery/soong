var annotated_dup =
[
    [ "Soong", null, [
      [ "Console", null, [
        [ "Command", null, [
          [ "EtlCommand", "class_soong_1_1_console_1_1_command_1_1_etl_command.html", "class_soong_1_1_console_1_1_command_1_1_etl_command" ],
          [ "MigrateCommand", "class_soong_1_1_console_1_1_command_1_1_migrate_command.html", "class_soong_1_1_console_1_1_command_1_1_migrate_command" ],
          [ "RollbackCommand", "class_soong_1_1_console_1_1_command_1_1_rollback_command.html", "class_soong_1_1_console_1_1_command_1_1_rollback_command" ],
          [ "StatusCommand", "class_soong_1_1_console_1_1_command_1_1_status_command.html", "class_soong_1_1_console_1_1_command_1_1_status_command" ],
          [ "YamlRecursiveFilterIterator", "class_soong_1_1_console_1_1_command_1_1_yaml_recursive_filter_iterator.html", "class_soong_1_1_console_1_1_command_1_1_yaml_recursive_filter_iterator" ]
        ] ]
      ] ],
      [ "Contracts", null, [
        [ "Data", null, [
          [ "DataProperty", "interface_soong_1_1_contracts_1_1_data_1_1_data_property.html", "interface_soong_1_1_contracts_1_1_data_1_1_data_property" ],
          [ "DataRecord", "interface_soong_1_1_contracts_1_1_data_1_1_data_record.html", "interface_soong_1_1_contracts_1_1_data_1_1_data_record" ]
        ] ],
        [ "Extractor", null, [
          [ "Extractor", "interface_soong_1_1_contracts_1_1_extractor_1_1_extractor.html", "interface_soong_1_1_contracts_1_1_extractor_1_1_extractor" ]
        ] ],
        [ "KeyMap", null, [
          [ "KeyMap", "interface_soong_1_1_contracts_1_1_key_map_1_1_key_map.html", "interface_soong_1_1_contracts_1_1_key_map_1_1_key_map" ]
        ] ],
        [ "Loader", null, [
          [ "Loader", "interface_soong_1_1_contracts_1_1_loader_1_1_loader.html", "interface_soong_1_1_contracts_1_1_loader_1_1_loader" ]
        ] ],
        [ "Task", null, [
          [ "EtlTask", "interface_soong_1_1_contracts_1_1_task_1_1_etl_task.html", "interface_soong_1_1_contracts_1_1_task_1_1_etl_task" ],
          [ "Task", "interface_soong_1_1_contracts_1_1_task_1_1_task.html", "interface_soong_1_1_contracts_1_1_task_1_1_task" ]
        ] ],
        [ "Transformer", null, [
          [ "Transformer", "interface_soong_1_1_contracts_1_1_transformer_1_1_transformer.html", "interface_soong_1_1_contracts_1_1_transformer_1_1_transformer" ]
        ] ]
      ] ],
      [ "Data", null, [
        [ "Property", "class_soong_1_1_data_1_1_property.html", "class_soong_1_1_data_1_1_property" ],
        [ "Record", "class_soong_1_1_data_1_1_record.html", "class_soong_1_1_data_1_1_record" ]
      ] ],
      [ "Extractor", null, [
        [ "ArrayExtractor", "class_soong_1_1_extractor_1_1_array_extractor.html", "class_soong_1_1_extractor_1_1_array_extractor" ],
        [ "CountableExtractorBase", "class_soong_1_1_extractor_1_1_countable_extractor_base.html", "class_soong_1_1_extractor_1_1_countable_extractor_base" ],
        [ "Csv", "class_soong_1_1_extractor_1_1_csv.html", "class_soong_1_1_extractor_1_1_csv" ],
        [ "DBAL", "class_soong_1_1_extractor_1_1_d_b_a_l.html", "class_soong_1_1_extractor_1_1_d_b_a_l" ],
        [ "ExtractorBase", "class_soong_1_1_extractor_1_1_extractor_base.html", "class_soong_1_1_extractor_1_1_extractor_base" ]
      ] ],
      [ "KeyMap", null, [
        [ "DBAL", "class_soong_1_1_key_map_1_1_d_b_a_l.html", "class_soong_1_1_key_map_1_1_d_b_a_l" ],
        [ "KeyMapBase", "class_soong_1_1_key_map_1_1_key_map_base.html", "class_soong_1_1_key_map_1_1_key_map_base" ]
      ] ],
      [ "Loader", null, [
        [ "Csv", "class_soong_1_1_loader_1_1_csv.html", "class_soong_1_1_loader_1_1_csv" ],
        [ "DBAL", "class_soong_1_1_loader_1_1_d_b_a_l.html", "class_soong_1_1_loader_1_1_d_b_a_l" ],
        [ "LoaderBase", "class_soong_1_1_loader_1_1_loader_base.html", "class_soong_1_1_loader_1_1_loader_base" ],
        [ "VarDump", "class_soong_1_1_loader_1_1_var_dump.html", "class_soong_1_1_loader_1_1_var_dump" ]
      ] ],
      [ "Task", null, [
        [ "EtlTask", "class_soong_1_1_task_1_1_etl_task.html", "class_soong_1_1_task_1_1_etl_task" ],
        [ "Task", "class_soong_1_1_task_1_1_task.html", "class_soong_1_1_task_1_1_task" ]
      ] ],
      [ "Tests", null, [
        [ "Contracts", null, [
          [ "Data", null, [
            [ "DataPropertyTestBase", "class_soong_1_1_tests_1_1_contracts_1_1_data_1_1_data_property_test_base.html", "class_soong_1_1_tests_1_1_contracts_1_1_data_1_1_data_property_test_base" ]
          ] ],
          [ "Extractor", null, [
            [ "CountableExtractorTestBase", "class_soong_1_1_tests_1_1_contracts_1_1_extractor_1_1_countable_extractor_test_base.html", "class_soong_1_1_tests_1_1_contracts_1_1_extractor_1_1_countable_extractor_test_base" ],
            [ "ExtractorTestBase", "class_soong_1_1_tests_1_1_contracts_1_1_extractor_1_1_extractor_test_base.html", "class_soong_1_1_tests_1_1_contracts_1_1_extractor_1_1_extractor_test_base" ]
          ] ],
          [ "KeyMap", null, [
            [ "KeyMapTestBase", "class_soong_1_1_tests_1_1_contracts_1_1_key_map_1_1_key_map_test_base.html", "class_soong_1_1_tests_1_1_contracts_1_1_key_map_1_1_key_map_test_base" ]
          ] ],
          [ "Loader", null, [
            [ "LoaderTestBase", "class_soong_1_1_tests_1_1_contracts_1_1_loader_1_1_loader_test_base.html", "class_soong_1_1_tests_1_1_contracts_1_1_loader_1_1_loader_test_base" ]
          ] ],
          [ "Transformer", null, [
            [ "TransFormerTestBase", "class_soong_1_1_tests_1_1_contracts_1_1_transformer_1_1_trans_former_test_base.html", "class_soong_1_1_tests_1_1_contracts_1_1_transformer_1_1_trans_former_test_base" ]
          ] ]
        ] ]
      ] ],
      [ "Transformer", null, [
        [ "Copy", "class_soong_1_1_transformer_1_1_copy.html", "class_soong_1_1_transformer_1_1_copy" ],
        [ "Double", "class_soong_1_1_transformer_1_1_double.html", "class_soong_1_1_transformer_1_1_double" ],
        [ "Increment", "class_soong_1_1_transformer_1_1_increment.html", "class_soong_1_1_transformer_1_1_increment" ],
        [ "KeyMapLookup", "class_soong_1_1_transformer_1_1_key_map_lookup.html", "class_soong_1_1_transformer_1_1_key_map_lookup" ],
        [ "TransformerBase", "class_soong_1_1_transformer_1_1_transformer_base.html", "class_soong_1_1_transformer_1_1_transformer_base" ],
        [ "UcFirst", "class_soong_1_1_transformer_1_1_uc_first.html", "class_soong_1_1_transformer_1_1_uc_first" ],
        [ "ValueLookup", "class_soong_1_1_transformer_1_1_value_lookup.html", "class_soong_1_1_transformer_1_1_value_lookup" ]
      ] ]
    ] ],
    [ "Countable", "class_countable.html", null ],
    [ "RecursiveFilterIterator", "class_recursive_filter_iterator.html", null ]
];