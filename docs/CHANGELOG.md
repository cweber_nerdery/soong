# Changelog

All notable changes to this project will be documented in this file.

Updates should follow the [Keep a CHANGELOG](http://keepachangelog.com/) principles.

This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html). For major version 0, we will increment the minor version for backward-incompatible changes.

## [Unreleased]

### Added
- Tests for `Extractor`, `KeyMap`, and `Loader` components.

## [0.4.0] - 2019-02-15

### Added
- `EtlTaskInterface::getLoader()`
- Tests for `Data` and `Transformer` components.

### Changed
- DBAL and Csv implementations have been moved:
    - `Soong\Csv\Extractor` -> `Soong\Extractor\Csv`
    - `Soong\Csv\Loader` -> `Soong\Loader\Csv`
    - `Soong\DBAL\Extractor` -> `Soong\Extractor\DBAL`
    - `Soong\DBAL\KeyMap` -> `Soong\KeyMap\DBAL`
    - `Soong\DBAL\Loader` -> `Soong\Extractor\Loader`
- `Interface` and `Trait` suffixes removed from all interfaces and traits.
- All interfaces moved into `Contracts` directory.
- All main components must now be created using `Class::create()` rather than `new`. This affects:
    - `DataPropertyInterface`
    - `DataRecordInterface`
    - `ExtractorInterface`
    - `KeyMapInterface`
    - `LoaderInterface`
    - `TaskInterface`
    - `TransformerInterface`
- Explicit component documentation pages have been removed in favor of Doxygen-generated documentation.
- Existing inline documentation has been cleaned up and expanded.

### Removed
- `KeyMapFactory`
- `MemoryKeyMap`

## [0.3.0] - 2019-02-05

### Added
- Added `getExtractor()` and `getAllTasks()` to task interfaces/implementations.
- Initial implementation of the `status` console command.
- All documentation moved into this repo, will be available on readthedocs.io.
- `DataPropertyInterface::isEmpty()`
- `DataRecordInterface::propertyExists()`

### Removed
- Removed subtask concept from task interfaces/implementations.
- Removed `CountableExtractorInterface`.

### Changed
- `DataRecordInterface::setProperty()` no longer accepts null values (only property objects with null values).
- `DataRecordInterface::getProperty()` no longer returns null values (only property objects with null values).
- `TransformerInterface::transform()` no longer accepts or returns null values (only property objects with null values).
- The `$configuration` parameter to `TransformerInterface::transform()` has been removed - configuration should be passed in the constructor instead.
- SHA256 for key hashing in `KeyMapBase`.
- Added configuration key for hash algorithm.

### Fixed
- Hashing serialized keys needs to make sure values are always strings.

## [0.2.1] - 2019-01-24

### Changed
- Merged all the repos back into one for ease of development.

## [0.1.0] - 2019-01-17

Initial release on Packagist.

[Unreleased]: https://gitlab.com/soongetl/soong/compare/0.4.0...master
[0.4.0]: https://gitlab.com/soongetl/soong/compare/0.3.0...0.4.0
[0.3.0]: https://gitlab.com/soongetl/soong/compare/0.2.1...0.3.0
[0.2.1]: https://gitlab.com/soongetl/soong/compare/0.1.0...0.2.1
[0.1.0]: https://gitlab.com/soongetl/soong/tree/0.1.0
