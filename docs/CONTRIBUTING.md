# Contributing

Contributions are **welcome** and will be fully **credited**. There's still a lot of refinement to be done to Soong - this is your opportunity to get involved with a new framework (and community) on the ground floor! As mentioned above, the plan is ultimately to break out components into small well-contained libraries - these will be excellent opportunities to get your feet wet maintaining your own open-source project.

There's plenty of work already identified in [the Gitlab issue queue](https://gitlab.com/soongetl/soong/issues). Feel free to browse, ask questions, and offer your own insights - or, if you have a migration itch you'd like to scratch and don't see an existing issue, open a new one.

## Working on issues

1. If you have an issue you'd like to work on, assign it to yourself.
1. If you haven't already, fork the project to your account.
1. Create a feature branch in your fork. Recommended branch name is `<gitlab issue #>-<dash-separated-issue-title>`
1. Develop your solution locally. Be sure to:
    - Make sure your changes are fully tested (see below).
    - Make sure your changes are fully documented (see below).
    - Follow the [PSR-2 Coding Standard](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md). Check the code style with `$ composer check-style` and fix it with `$ composer fix-style`.
1. Make sure each individual commit in your pull request is meaningful. If you had to make multiple intermediate commits while developing, please [squash them](http://www.git-scm.com/book/en/v2/Git-Tools-Rewriting-History#Changing-Multiple-Commit-Messages) before submitting.
1. Commits should reference the issue number - e.g., a commit for [Add community docs up front](https://gitlab.com/soongetl/soong/issues/51) might have the commit message "#51: Expand community documentation and move to docs directory.".
1. On gitlab, create a merge request and submit it. 

## Tests
Automated tests are critical, especially when code is changing rapidly. They help ensure that any changes made don't produce any unexpected consequences, and give confidence that a new piece of code does what it's expected to do. In the Soong `tests` directory, you'll find existing tests laid out in parallel with the `src` directory. Of particular note is `tests/Contracts` - while interfaces can't be tested (since they don't do anything worth testing), we do provide base classes here which you should extend for the tests of your components - these will give you testing that your components meet the documented expectations of the interfaces, so in writing tests you can focus on the specific features added by your own code.

To run the test suite locally:

``` bash
$ composer test
```

## Documentation

* Classes and methods are to be fully documented in comment blocks - these are used to automatically generate the API Reference section of the [online documentation](https://soong-etl.readthedocs.io/).
* Add any non-trivial changes you've made to docs/CHANGELOG.md.
* Review `README.md` and any `.md` files under `docs` to see if any changes are missing. Don't miss docs/api/doxygen_home.md (which is used as the homepage for the Doxygen-generated API docs).
